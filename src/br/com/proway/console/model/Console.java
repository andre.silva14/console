package br.com.proway.console.model;

public class Console {
    public String nome;
    public String marca;
    public String modelo;
    public double preco;
    public double memoria;
    public boolean disponivel = true;
    public int estoque;

}
